# -*- coding: utf-8 -*-
from flask import Flask, abort
from flask_restful import Resource, Api, reqparse

import json

# import self service py
from service.global_service import GlobalDev


# ========初怡化程式========
app = Flask(__name__)

# set config class
app.config.from_object('config.DevConfig')

# set api
api = Api(app)

# get config parameters
web_url = app.config['WEB_URI']
sql_config = app.config['SQL_CONFIG']

# test json data
todos = {
    'todo1': {'task': 'build an API'},
    'todo2': {'task': '?????'},
    'todo3': {'task': 'profit!'},
}


def abortIfTodoDoesntExist(todo_id):
    """
    check id
    :param todo_id:
    """
    if todo_id not in todos:
        abort(404, message="Todo {} doesn't exist".format(todo_id))




class sample_api_id(Resource):

    def get(self, todo_id):
        """
        get detail api
        :param todo_id:
        :return:
        """
        abortIfTodoDoesntExist(todo_id)
        return todos[todo_id]

    def delete(self, todo_id):
        """
        delete api
        :param todo_id:
        :return:
        """
        abortIfTodoDoesntExist(todo_id)
        del todos[todo_id]
        return '', 204

    def put(self, todo_id):
        """
        upsert api
        :param todo_id:
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('todo_id', type=str)

        args = parser.parse_args()
        task = {'task': args['task']}
        todos[todo_id] = task
        return task, 201



class sample_api(Resource):

    def get(self):
        """
        get hello url (reed)
        :return:
        """
        ser = GlobalDev(url=web_url)
        u = ser.get_url()

        return u


    def post(self):
        """
        get parameter then do something api (edit)
        :return:
        """
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('name', type=str)
            parser.add_argument('g', type=str)
            parser.add_argument('o', type=int)

            args = parser.parse_args()
            name = args['name']
            gender = args['g']
            old = args['o']

            return json.dumps({'name': name, 'gender': gender, 'old': old})

        except Exception as ex:
            return str(ex), 500


##
## Actually setup the Api resource routing here
##
api.add_resource(sample_api, '/')
api.add_resource(sample_api_id, '/<todo_id>')


if __name__ == '__main__':
    app.run()
