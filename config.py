# -*- coding: utf-8 -*-
class Config(object):

    SQL_CONFIG = {
        'host': '',
        'connect_timeout': 60,
        'read_timeout': 60,
        'write_timeout': 60,
        'max_allowed_packet': 102400,
        'user': '',
        'password': '',
        'db': '',
        'charset': 'utf8mb4'
    }

    GCS_JSONKEY = 'gcs.json'
    DOWNLOAD_PATH = 'static/filedownload/'

    LOG_URL = 'mongodb://127.0.0.1:27017'
    LOG_DB = ''
    LOG_COLLECTION = ''
    LOG_SMS_COLLECTION = ''



class ProdConfig(Config):
    WEB_URI = 'https://test.com.tw'

class DevConfig(Config):
    DEBUG = True
    WEB_URI = 'http://127.0.0.1:5000'
